# Löve Chaos Prototype

A turn based strategy game with a twist. Players take turns in parallel,
this removes the first mover advantage and adds a source of randomness.


## Lessons Learned

 * Parallel turns add possibilities for risk/reward moves.
 * It does not offer skill potential and thus should not be a main focus.
 * Encurages controling ground instead of direct conflicts.
 * Maps require unwalkable fields to create chokes, borders, etc.
 * Should not be to many units (> 10), just moving 6 can be a chore.
 * Will require additional incentives to prevent boing passive games.
 * Will likely require AOE to end games.


## Future improvements

 * Actually implement some sort of ai.
 * More intresting generated maps.
