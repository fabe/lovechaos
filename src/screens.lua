-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local screens = {}

function screens.update()
    -- update all screens, bottom to top
    for i, screen in ipairs(screens) do
        screen:update()
    end
end

function screens.draw()
    -- draw all screens, bottom to top
    for i, screen in ipairs(screens) do
        screen:draw()
    end
end

function screens.peek()
    return screens[screens:size()]
end

function screens.size()
    return #screens
end

function screens.push(screen)

    -- deactivate prev screen
    local prev_screen = screens:peek()
    if prev_screen then
        prev_screen:deactivate() -- TODO recursivly deactivate children?
    end

    -- push and activate screen
    table.insert(screens, screen)
    screen:activate() -- TODO recursivly activate children?

    return screens:size()
end

function screens.pop()

    -- pop and deactivate prev screen
    local prev_screen = screens:peek()
    table.remove(screens, screens:size())
    if prev_screen then
        prev_screen:deactivate() -- TODO recursivly deactivate children?
    end

    -- activate now top screen
    local curr_screen = screens:peek()
    if curr_screen then
        curr_screen:activate() -- TODO recursivly activate children?
    end

    return prev_screen
end

function screens.clear()
    while screens:pop() do end
end

function screens.replace(screen)
    local prev_screen = screens:pop()
    screens:push(screen)
    return prev_screen
end

return screens
