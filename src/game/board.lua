-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local Signal = require('hump.signal')
local Class = require('hump.class')
local gfx = require("src.gfx")
local util = require("src.util")

local Board = Class{
    WIDTH = 13,
    HEIGHT = 8,
    MAX_HP = 3,
    DAMMAGE = 1
}

function Board:init()
    self.actions = {} -- current actions to create the next state
    self.history = {} -- all previous actions
    self.selected = nil
    self.selecting_move = false
    self.selecting_attack = false
    self.unwalkable_fields = {
        {8, 1}, {9, 1}, {10, 1},
        {6, 4}, {7, 4}, {8, 4},
        {6, 5}, {7, 5}, {8, 5},
        {4, 8}, {5, 8}, {6, 8},
    }

    self:_createInitialPieces()
    self:_createHandlers()
end

function Board:_createInitialPieces()
    self.pieces = {
    
        -- player on left half
        {hp=Board.MAX_HP, player=true, pos={2, 3}, action=nil},
        {hp=Board.MAX_HP, player=true, pos={2, 4}, action=nil},
        {hp=Board.MAX_HP, player=true, pos={3, 4}, action=nil},
        {hp=Board.MAX_HP, player=true, pos={2, 5}, action=nil},
        {hp=Board.MAX_HP, player=true, pos={3, 5}, action=nil},
        {hp=Board.MAX_HP, player=true, pos={2, 6}, action=nil},

        -- cpu on right half
        {hp=Board.MAX_HP, player=false, pos={12, 3}, action=nil},
        {hp=Board.MAX_HP, player=false, pos={12, 4}, action=nil},
        {hp=Board.MAX_HP, player=false, pos={11, 4}, action=nil},
        {hp=Board.MAX_HP, player=false, pos={12, 5}, action=nil},
        {hp=Board.MAX_HP, player=false, pos={11, 5}, action=nil},
        {hp=Board.MAX_HP, player=false, pos={12, 6}, action=nil},
    }
end

function Board:_createHandlers()
    self._onSelectAttack = function()
        self.selecting_attack = true
    end
    self._onSelectMove = function()
        self.selecting_move = true
    end
    self._onSelectCancel = function()
        self:_cancel() 
    end
    self._onSelectEndTurn = function()
        self:_endTurn()
    end
end

function Board:_endTurn()
    self.selected = nil
    self.selecting_move = nil
    self.selecting_attack = nil
    self:_aiPieceActions()
    self.pieces = self:_movePieces(self.pieces)
    self.pieces = self:_resolveAttacks(self.pieces)
    self:_checkGameOver()
end

function Board:_nonMoving(unresolved)
    local resolved = {}
    local temp = {}
    for i, piece in ipairs(unresolved) do
        if not piece.action or piece.action.name ~= "move" then
            table.insert(resolved, piece)
        else
            table.insert(temp, piece)
        end
    end
    return resolved, temp
end

function Board:_moveConflict(resolved, unresolved)
    local temp = {}
    local found = {}
    for i, piece in ipairs(unresolved) do
        local conflict = false
        for j, _piece in ipairs(unresolved) do
            if not util.posEqual(piece.pos, _piece.pos) then
                local pos = piece.action.pos
                local _pos = _piece.action.pos
                if util.posEqual(pos, _pos) then
                    conflict = true
                end
            end
        end
        if conflict then
            table.insert(found, piece)
        else
            table.insert(temp, piece)
        end
    end
    for i, piece in ipairs(found) do
        local target_pos = piece.action.pos
        piece.action = nil
        table.insert(resolved, piece)
        Signal.emit("board_move_failed", piece, target_pos)
    end
    return resolved, temp
end

function Board:_moveToOccupied(resolved, unresolved)
    local found_instance = false
    local temp = {}
    for i, piece in ipairs(unresolved) do
        target_pos = piece.action.pos
        if self:getPiece(resolved, target_pos) then
            piece.action = nil
            table.insert(resolved, piece)
            Signal.emit("board_move_failed", piece, target_pos)
            found_instance = true
        else
            table.insert(temp, piece)
        end
    end
    if found_instance then -- new field now occupied
        return self:_moveToOccupied(resolved, temp)
    else
        return resolved, temp
    end
end

function Board:_moveSuccessful(resolved, unresolved)
    for i, piece in ipairs(unresolved) do
        local prev_pos = piece.pos
        piece.pos = piece.action.pos
        piece.action = nil
        table.insert(resolved, piece)
        Signal.emit("board_move_successful", piece, prev_pos)
        -- TODO emit dodge signal if unit attacked prev_pos
    end
    return resolved
end

function Board:_movePieces(unresolved)
    local resolved, unresolved = self:_nonMoving(unresolved)
    local resolved, unresolved = self:_moveConflict(resolved, unresolved)
    local resolved, unresolved = self:_moveToOccupied(resolved, unresolved)
    return self:_moveSuccessful(resolved, unresolved)
end

function Board:_resolveAttacks(unresolved)
    local resolved = {}

    for i, piece in ipairs(unresolved) do
        if piece.action and piece.action.name == "attack" then
            local target_pos = piece.action.pos
            local victim = self:getPiece(unresolved, target_pos)
            piece.action = nil
            if victim then
                victim.hp = victim.hp - Board.DAMMAGE
                Signal.emit("board_attack_successful", piece, victim)
            else
                Signal.emit("board_attack_failed", piece, target_pos)
            end
        end
    end
    
    -- filter victims
    for i, piece in ipairs(unresolved) do
        if piece.hp > 0 then
            table.insert(resolved, piece)
        else
            Signal.emit("board_piece_died", piece)
        end
    end

    return resolved
end

function Board:_checkGameOver()
    local player_pieces = false
    local enemie_pieces = false
    for i, piece in ipairs(self.pieces) do
        if piece.player then
            player_pieces = true
        else
            enemie_pieces = true
        end
    end
    if not player_pieces and not enemie_pieces then
        print("GAME OVER: DRAW!")
        Signal.emit("board_game_over", "draw")
    elseif player_pieces and not enemie_pieces then
        print("GAME OVER: PLAYER WINS!")
        Signal.emit("board_game_over", "player")
    elseif not player_pieces and enemie_pieces then
        print("GAME OVER: COMPUTER WINS!")
        Signal.emit("board_game_over", "computer")
    else
        Signal.emit("board_new_turn") -- game not over
    end
end

function Board:_cancel()
    if self.selecting_move or self.selecting_attack then
        self.selecting_move = false
        self.selecting_attack = false
    else
        self.selected.action = nil
    end
end

function Board:registerHandlers()
    Signal.register('player_select_attack', self._onSelectAttack)
    Signal.register('player_select_move', self._onSelectMove)
    Signal.register('player_select_cancel', self._onSelectCancel)
    Signal.register('player_select_endturn', self._onSelectEndTurn)
end

function Board:removeHandlers()
    Signal.remove('player_select_attack', self._onSelectAttack)
    Signal.remove('player_select_move', self._onSelectMove)
    Signal.remove('player_select_cancel', self._onSelectCancel)
    Signal.remove('player_select_endturn', self._onSelectEndTurn)
end

function Board:canEndTurn()
    return true -- always true
end

function Board:canAttack()
    return (
        self.selected and 
        not self.selecting_move and 
        not self.selecting_attack
    )
end

function Board:canMove()
    return (
        self.selected and 
        not self.selecting_move and 
        not self.selecting_attack
    )
end

function Board:canCancel()
    return (
        self.selected and (
            self.selecting_move or
            self.selecting_attack or 
            self.selected.action
        )
    )
end

function Board:_aiPieceActions()
    for i, piece in ipairs(self.pieces) do
        if not piece.player then
            self:_aiPieceAction(piece)
        end
    end
end

function Board:_aiPieceAction(piece)
    local action = love.math.random(3)
    local reachable = self:getReachable(piece.pos)
    if action == 1 then -- 1/3 chance of move
        local move_choice = love.math.random(#reachable)
        piece.action = {pos=reachable[move_choice], name="move"}
    elseif action == 2 then -- 1/3 chance of attack
        local attack_choice = love.math.random(#reachable)
        piece.action = {pos=reachable[attack_choice], name="attack"}
    else  -- 1/3 chance of idle
        piece.action = nil
    end
end

function Board:selectField(pos)
    local piece = self:getPiece(self.pieces, pos)

    -- select piece
    if (piece and piece.player and not self.selecting_move and
            not self.selecting_attack) then
        self.selected = piece
        self.selecting_move = false
        self.selecting_attack = false

    -- set piece move action
    elseif self.selecting_move and self:isReachable(pos) then
        self.selected.action = {pos=pos, name="move"}
        self.selecting_move = false

    -- set piece attack action
    elseif self.selecting_attack and self:isReachable(pos) then
        self.selected.action = {pos=pos, name="attack"}
        self.selecting_attack = false
    end
end

function Board:getPiece(pieces, pos)
    for i, piece in ipairs(pieces) do
        if util.posEqual(piece.pos, pos) then
            return piece
        end
    end
    return nil
end

function Board:isWalkable(pos)
    local x, y = unpack(pos)

    if x < 1 or x > Board.WIDTH or y < 1 or y > Board.HEIGHT then
        return false
    end

    for i, unwalkable_pos in ipairs(self.unwalkable_fields) do
        local ux, uy = unpack(unwalkable_pos)
        if ux == x and uy == y then
            return false
        end
    end

    return true
end

function Board:isReachable(pos)
    if self.selected then
        local reachable = self:getReachable(self.selected.pos)
        for i, reachable_pos in ipairs(reachable) do
            if util.posEqual(pos, reachable_pos) then
                return true
            end
        end
    end
    return false
end

function Board:getReachable(pos)
    local x, y = unpack(pos)
    local reachable = {}
    local entries = {
        {x - 1, y - 1}, {x, y - 1}, {x + 1, y - 1}, {x - 1, y},
        {x + 1, y}, {x - 1, y + 1}, {x, y + 1}, {x + 1, y + 1}
    }
    for i, entry in ipairs(entries) do
        if self:isWalkable(entry) then
            table.insert(reachable, entry)
        end
    end
    return reachable
end

return Board
