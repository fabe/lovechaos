-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local Signal = require('hump.signal')
local gfx = require("src.gfx")

local input = {}

function input.status()
    local pressed = love.mouse.isDown(1)
    return gfx.screenToGfxPos({love.mouse.getPosition()}), pressed
end

function love.mousereleased(scr_x, scr_y, button, istouch)
    if button == 1 then
        Signal.emit("input_select", gfx.screenToGfxPos({scr_x, scr_y}))
    end
end

function love.keyreleased(key)
    if key == 'escape' then
        Signal.emit("input_back")
    end
end

return input
