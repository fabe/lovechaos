-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local vector_light = require "hump.vector-light"

local gfx = {

    -- internal resolution
    WIDTH = 256,
    HEIGHT = 160,

    -- default window settings (desktop)
    WIN_WIDTH = 768,
    WIN_HEIGHT = 480,

    font_small = nil,
    font_medium = nil,
    font_large = nil,
}

-- TODO read all png files from dir; filename - extension = name
-- TODO read all json info files from dir; filename - extension = name
local INFO = { 
    highlight={
        path="data/sprites/highlight.png",
        frames=16, animations=5, duration=1
    },
    actions={
        path="data/sprites/actions.png",
        frames=1, animations=2, duration=1
    },
    unit={
        path="data/sprites/unit.png",
        frames=2, animations=2, duration=0.5
    },
    buttons={
        path="data/sprites/buttons.png",
        frames=1, animations=12, duration=0.5
    },
    background={
        path="data/backgrounds/game.png",
        frames=1, animations=1, duration=1
    },
}

local sprites = {}

function gfx.load()

    -- setup window
    love.window.setTitle("Löve Chaos")
    local flags = {resizable=true, vsync=true, msaa=0}
    love.window.setMode(gfx.WIN_WIDTH, gfx.WIN_HEIGHT, flags)

    -- TODO add loading screen here

    -- load
    for name, info in pairs(INFO) do

        local img = love.graphics.newImage(info["path"])
        local frames = info["frames"]
        local animations = info["animations"]
        local iw, ih = img:getDimensions()
        img:setFilter("nearest")

        -- sprite quads lookup table
        local quads = {}
        for x=1, info["frames"] do
            quads[x] = {}
            for y=1, info["animations"] do
                local qw = iw / frames
                local qh = ih / animations
                local qx = qw * (x - 1)
                local qy = qh * (y - 1)
                quads[x][y] = love.graphics.newQuad(qx, qy, qw, qh, iw, ih)
            end
        end

        sprites[name] = {
            img=img,
            quads=quads,
            frames=frames,
            animations=animations,
            duration=info["duration"],
        }
    end

    -- load fonts
    gfx.font_small = love.graphics.newFont(8)
    gfx.font_small:setFilter("nearest", "nearest", 1)
    gfx.font_medium = love.graphics.newFont(16)
    gfx.font_medium:setFilter("nearest", "nearest", 1)
    gfx.font_large = love.graphics.newFont(32)
    gfx.font_large:setFilter("nearest", "nearest", 1)
end

function gfx.scale()
    local sx = love.graphics.getWidth() / gfx.WIDTH
    local sy = love.graphics.getHeight() / gfx.HEIGHT
    love.graphics.scale(sx, sy)
end

function gfx.draw(pos, name, animation, r)
    -- TODO add horizontal mirror option
    -- TODO add vertical mirror option
    -- TODO add animation modes (loop, bounce)
    -- TODO add seed. current time as seed => start at first frame

    -- set default values
    local r = r or 0
    local animation = animation or 1

    -- get animation frame to draw
    local sprite = sprites[name]
    local frame_duration = sprite.duration / sprite.frames
    local time = (love.timer.getTime() % sprite.duration)
    local frame = math.floor(time / frame_duration) + 1

    -- position top-left and rotate in place
    local quad = sprite.quads[frame][animation]
    local qx, qy, qw, qh = quad:getViewport()
    local ox, oy = qw / 2, qh / 2
    local x, y = unpack(pos)
    love.graphics.draw(sprite.img, quad, x + ox, y + oy, r, 1, 1, ox, oy)
end

function gfx.screenToGfxX(scr_x)
    local scr_w = love.graphics.getWidth()
    return math.floor((gfx.WIDTH * scr_x) / scr_w)
end

function gfx.screenToGfxY(scr_y)
    local scr_h = love.graphics.getHeight()
    return math.floor((gfx.HEIGHT * scr_y) / scr_h)
end

function gfx.screenToGfxW(scr_w)
    return gfx.screenToGfxX(scr_w)
end

function gfx.screenToGfxH(scr_h)
    return gfx.screenToGfxY(scr_h)
end

function gfx.screenToGfxPos(pos)
    local x, y = unpack(pos)
    return {gfx.screenToGfxX(x), gfx.screenToGfxY(y)}
end

function gfx.screenToGfxRect(rect)
    local x, y, w, h = unpack(rect)
    return {
        gfx.screenToGfxX(x), gfx.screenToGfxY(y),
        gfx.screenToGfxW(w), gfx.screenToGfxH(h)
    }
end

return gfx
