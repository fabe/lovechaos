-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local Class = require('hump.class')
local Signal = require('hump.signal')
local Board = require("src.game.board")
local gfx = require("src.gfx")
local screens = require("src.screens")
local vector = require("hump.vector-light")
local Button = require("src.ui.button")

local FIELD_W = gfx.WIDTH / 16
local FIELD_H = gfx.HEIGHT / 10
local OFFSET_X = FIELD_W * 0.5
local OFFSET_Y = FIELD_H * 1.5

local GameScreen = Class{}

function GameScreen:init()
    self.board = Board()
    self.button_endturn = Button(
        {228, 4, 24, 16}, "player_select_endturn",
        function() return self.board:canEndTurn() end,
        {"buttons", 10}, {"buttons", 11}, {"buttons", 12}
    )
    self.button_attack = Button(
        {228, 104, 24, 16}, "player_select_attack",
        function() return self.board:canAttack() end,
        {"buttons", 1}, {"buttons", 2}, {"buttons", 3}
    )
    self.button_move = Button(
        {228, 120, 24, 16}, "player_select_move",
        function() return self.board:canMove() end,
        {"buttons", 4}, {"buttons", 5}, {"buttons", 6}
    )
    self.button_cancel = Button(
        {228, 136, 24, 16}, "player_select_cancel",
        function() return self.board:canCancel() end,
        {"buttons", 7}, {"buttons", 8}, {"buttons", 9}
    )
    self:_createHandlers()
end

function GameScreen:_createHandlers()
    self._onInputSelect = function(pos)
        self.board:selectField(self:gfxToBoardPos(pos))
    end
    self._onInputBack = function()
        if self.board.selected then
            self.board.selected = nil
            self.board.selecting_move = false
            self.board.selecting_attack = false
        else
            screens:clear()
        end
    end
end

function GameScreen:activate()
    Signal.register('input_select', self._onInputSelect)
    Signal.register('input_back', self._onInputBack)
    self.button_attack:registerHandlers()
    self.button_move:registerHandlers()
    self.button_cancel:registerHandlers()
    self.button_endturn:registerHandlers()
    self.board:registerHandlers()
end

function GameScreen:deactivate()
    Signal.remove('input_select', self._onInputSelect)
    Signal.remove('input_back', self._onInputBack)
    self.button_attack:removeHandlers()
    self.button_move:removeHandlers()
    self.button_cancel:removeHandlers()
    self.button_endturn:removeHandlers()
    self.board:removeHandlers()
end

function GameScreen:update()
end

function GameScreen:_drawActionMenu()
    self.button_endturn:draw()
    self.button_attack:draw()
    self.button_move:draw()
    self.button_cancel:draw()
end

function GameScreen:_highlight(pos, animation)
    local gfx_pos = self:boardToGfxPos(pos)
    gfx.draw(gfx_pos, "highlight", animation)
end

function GameScreen:_highlightSelected()
    if (self.board.selected and not self.board.selecting_move and 
            not self.board.selecting_attack) then
        self:_highlight(self.board.selected.pos, 3)
    end
end

function GameScreen:_highlightMovable()
    if self.board.selected and self.board.selecting_move then
        local reachable = self.board:getReachable(self.board.selected.pos)
        for i, pos in ipairs(reachable) do
            self:_highlight(pos, 2)
        end
    end
end

function GameScreen:_highlightAttackable()
    if self.board.selected and self.board.selecting_attack then
        local reachable = self.board:getReachable(self.board.selected.pos)
        for i, pos in ipairs(reachable) do
            self:_highlight(pos, 4)
        end
    end
end

function GameScreen:_drawPeaces()
    for i, piece in ipairs(self.board.pieces) do

        -- draw unit
        if piece.player then
            gfx.draw(self:boardToGfxPos(piece.pos), "unit", 1)
        else
            gfx.draw(self:boardToGfxPos(piece.pos), "unit", 2)
        end

        -- draw hp  TODO color
        local hp_ratio = piece.hp / self.board.MAX_HP
        local hprect = {piece.pos[1], piece.pos[2], hp_ratio, 1.0 / 8.0}
        love.graphics.rectangle("fill", unpack(self:boardToGfxRect(hprect)))

    end
end

function GameScreen:_drawActions()
    local animations = {move=1, attack=2}
    for i, piece in ipairs(self.board.pieces) do
        if piece.action then
            local px, py = unpack(self:boardToGfxPos(piece.pos))
            local tx, ty = unpack(self:boardToGfxPos(piece.action.pos))
            local gfx_pos = {px + ((tx - px) / 2), py + ((ty - py) / 2)}
            local r = vector.angleTo(tx - px, ty - py, 1.0, 0.0)
            gfx.draw(gfx_pos, "actions", animations[piece.action.name], r)
        end
    end
end

function GameScreen:gfxToBoardX(x)
    return math.floor((x - OFFSET_X) / FIELD_W) + 1
end

function GameScreen:gfxToBoardY(y)
    return math.floor((y - OFFSET_Y) / FIELD_H) + 1
end

function GameScreen:gfxToBoardW(w)
    return math.floor(w / FIELD_W)
end

function GameScreen:gfxToBoardH(h)
    return math.floor(h / FIELD_H)
end

function GameScreen:gfxToBoardPos(pos)
    local x, y = unpack(pos)
    return {self:gfxToBoardX(x), self:gfxToBoardY(y)}
end

function GameScreen:gfxToBoardRect(rect)
    local x, y, w, h = unpack(rect)
    return {
        self:gfxToBoardX(x), self:gfxToBoardY(y),
        self:gfxToBoardW(w), self:gfxToBoardH(h)
    }
end

function GameScreen:boardToGfxX(x)
    return OFFSET_X + (x - 1) * FIELD_W
end

function GameScreen:boardToGfxY(y)
    return OFFSET_Y + (y - 1) * FIELD_H
end

function GameScreen:boardToGfxW(w)
    return FIELD_W * w
end

function GameScreen:boardToGfxH(h)
    return FIELD_H * h
end

function GameScreen:boardToGfxPos(pos)
    local x, y = unpack(pos)
    return {self:boardToGfxX(x), self:boardToGfxY(y)}
end

function GameScreen:boardToGfxRect(rect)
    local x, y, w, h = unpack(rect)
    return {
        self:boardToGfxX(x), self:boardToGfxY(y),
        self:boardToGfxW(w), self:boardToGfxH(h)
    }
end

function GameScreen:draw()
    gfx.draw({0, 0}, "background")
    self:_highlightSelected()
    self:_highlightMovable()
    self:_highlightAttackable()
    self:_drawPeaces()
    self:_drawActions()
    self:_drawActionMenu()
end

return GameScreen
