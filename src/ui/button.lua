-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local Class = require('hump.class')
local Signal = require('hump.signal')
local gfx = require("src.gfx")
local input = require("src.input")
local util = require("src.util")

Button = Class{}

function Button:init(rect, select_signal_name, isEnabledCallback,
                    spriteinfo_unpressed, spriteinfo_pressed,
                    spriteinfo_deactivated)

    -- FIXME pass pos instead of rect, deduce w & h from spriteinfo quads
    -- FIXME assert all spriteinfo quads have same w, h
    self.rect = rect
    self._select_signal_name = select_signal_name
    self._spriteinfo_pressed = spriteinfo_pressed
    self._spriteinfo_unpressed = spriteinfo_unpressed
    self._spriteinfo_deactivated = spriteinfo_deactivated
    self._isEnabledCallback = isEnabledCallback
    self:_createHandlers()
end

function Button:_isPressed()
    local pos, pressed = input.status()
    return pressed and util.isCollision(self.rect, pos)
end

function Button:_createHandlers()
    self._onInputSelect = function(pos)
        if (self:_isEnabledCallback() and 
                util.isCollision(self.rect, pos)) then
            Signal.emit(self._select_signal_name)
        end
    end
end

function Button:registerHandlers()
    Signal.register('input_select', self._onInputSelect)
end

function Button:removeHandlers()
    Signal.remove('input_select', self._onInputSelect)
end

function Button:draw()
    local spriteinfo = self._spriteinfo_deactivated
    if self:_isEnabledCallback() then
        if self:_isPressed() then
            spriteinfo = self._spriteinfo_pressed
        else
            spriteinfo = self._spriteinfo_unpressed
        end
    end
    local x, y, w, h = unpack(self.rect)
    gfx.draw({x, y}, unpack(spriteinfo))
end

return Button
