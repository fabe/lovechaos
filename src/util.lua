-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local util = {}

function util.posEqual(pos_a, pos_b) -- FIXME use hump vector
    local ax, ay = unpack(pos_a)
    local bx, by = unpack(pos_b)
    return ax == bx and ay == by
end

function util.isCollision(rect, point)
    px, py = unpack(point)
    rx, ry, rw, rh = unpack(rect)
    return px >= rx and px < (rx + rw) and py >= ry and py < (ry + rh)
end

return util
