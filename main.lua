-- Copyright (c) 2017 Fabian Barkhau <f483@protonmail.com>
-- License: MIT (see LICENSE file)

local gfx = require("src.gfx")
local GameScreen = require("src.game.screen")
local input = require("src.input")
local screens = require("src.screens")

-- STYLE GUIDE
-- Naming: CONSTANT_NAME, variable_name, functionName, ClassName
-- Otherwise be pythonic/pep8

-- POSITIONAL NOTATION
-- {scr_x, scr_y, scr_w, scr_h} actual screen position
-- {gfx_x, gfx_y, gfx_w, gfx_h} internal gfx position
-- {brd_x, brd_y, brd_w, brd_h} game board position

function love.load()
    gfx:load()
    screens.push(GameScreen())
end

function love.update()
    if screens:size() == 0 then
        love.event.quit()
    end
    screens:update()
end

function love.draw()
    gfx:scale()
    screens:draw()
end

